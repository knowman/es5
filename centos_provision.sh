#!/usr/bin/env bash
 
sudo yum -y update
 
if [ ! -f /var/log/vmsetup ];
then
sudo yum -y install wget
sudo yum -y install curl 


# java
cd ~

wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm"
sudo yum -y localinstall jdk-8u60-linux-x64.rpm
rm ~/jdk-8u60-linux-x64.rpm
export JAVA_HOME=/usr/java/jdk1.8.0_60/jre  


#elasticsearch  
cd ~
sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.0.rpm
sudo rpm --install elasticsearch-5.2.0.rpm

sudo -i service elasticsearch start
#sudo systemctl daemon-reload
#sudo systemctl enable elasticsearch.service

# configure elasticsearch
cp /vagrant/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
sudo mkdir /etc/elasticsearch/analysis
sudo cp /vagrant/synonyms.txt /etc/elasticsearch/analysis
sudo -i service elasticsearch restart

# install marvel and sense
#cd /usr/share/elasticsearch
#sudo bin/plugin -i elasticsearch/marval/latest
 
#application
sudo yum install -y git

#
touch /var/log/vmsetup
fi
#
sudo -i service elasticsearch restart
#sudo systemctl start elasticsearch.service
#
